using System;
using Microsoft.SPOT;

namespace CashRegisterClient
{
    class UpdatePointsResponse
    {
        public int statusCode { get; set; }

        public UpdatePointsResponse() { }

        public UpdatePointsResponse(int s)
        {
            this.statusCode = s;
        }
    }
}
