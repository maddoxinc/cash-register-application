using System;
using System.Net;
using Microsoft.SPOT;
using Microsoft.SPOT.Net;
using Microsoft.SPOT.Hardware;
using Gadgeteer.Networking;
using GT = Gadgeteer;
using GTM = Gadgeteer.Modules;
using GHI.Networking;
using GHIElectronics.Gadgeteer;
using Gadgeteer.Modules.GHIElectronics;
using Json.NETMF;
using System.Text;
using System.IO;
using System.Net.Sockets;
using GHI.Utilities;

namespace CashRegisterClient
{
    static class Functions
    {
        static public string handleResponse(WebRequest req)
        {
            string response = null;
            using (var res = req.GetResponse() as HttpWebResponse)
            {
                if (res.StatusCode == HttpStatusCode.OK)
                {
                    using (var stream = res.GetResponseStream())
                    {
                        if (stream != null)
                        {
                            var reader = new StreamReader(stream);
                            response = reader.ReadToEnd();
                            if (response != null)
                                return response;
                            else
                                return null;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
                else
                {
                    return null;
                }
            }
        }

        static public Socket connect(String conn_name, String address, int port)
        {
            Socket sock;
            IPEndPoint iep;

            try
            {

                IPAddress ipa = IPAddress.Parse(address);
                iep = new IPEndPoint(ipa, port);
                sock = null;
                try
                {
                    sock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                }
                catch (SystemException)
                {
                    return null;
                }

                try
                {
                    sock.Connect(iep);
                }
                catch (System.Net.Sockets.SocketException)
                {
                    return null;
                }
            }
            catch (Exception) { return null; }

            return sock;
        }

        public static int send(Stream stream, Socket sock)
        {
            int actual_send = 0, res;
            byte[] bufferbytes;

            try
            {
                bufferbytes = ((MemoryStream)stream).ToArray();
                res = send_int(sock, bufferbytes.Length);
                if (res < 0)
                    return -1;

                while (actual_send != bufferbytes.Length)
                {
                    actual_send += sock.Send(bufferbytes, actual_send, bufferbytes.Length - actual_send, 0);
                }
            }
            catch (System.Net.Sockets.SocketException ex)
            {
                Debug.Print(ex.Message);
                return -1;
            }

            return actual_send;
        }

        public static int send_int(Socket sock, int n)
        {
            byte[] bufn = new byte[4];
            int res;

            try
            {
                bufn = BitConverter.GetBytes(n);
                if (BitConverter.IsLittleEndian)
                {
                    bufn = ReverseBytes(bufn);
                }
                res = sock.Send(bufn, 4, 0);
            }
            catch (Exception)
            {
                return -1;
            }
            return res;
        }

        public static byte[] ReverseBytes(byte[] inArray)
        {
            byte temp;
            int highCtr = inArray.Length - 1;

            for (int ctr = 0; ctr < inArray.Length / 2; ctr++)
            {
                temp = inArray[ctr];
                inArray[ctr] = inArray[highCtr];
                inArray[highCtr] = temp;
                highCtr -= 1;
            }
            return inArray;
        }

        public static bool sendBitmap(Bitmap bitmap, Socket sock)
        {
            try
            {
                //byte[] bitmap_array = new byte[bitmap.Width * bitmap.Height * 3 + 54];
                //ConvertToFile(bitmap, bitmap_array);
                //byte[] bitmap_array = new byte[bitmap.Width * bitmap.Height * 2];
                //GHI.Utilities.Bitmaps.GetBuffer(bitmap, bitmap_array);
                byte[] bitmap_array = bitmap.GetBitmap();
                MemoryStream stream_bitmap = new MemoryStream(bitmap_array);
                send(stream_bitmap, sock);
            }
            catch (Exception e)
            {
                Debug.Print(e.Message);
                return false;
            }

            return true;
        }
    }
}
