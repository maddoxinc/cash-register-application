﻿using System;
using System.Collections;
using System.Threading;
using System.Net;
using Microsoft.SPOT;
using Microsoft.SPOT.Presentation;
using Microsoft.SPOT.Presentation.Controls;
using Microsoft.SPOT.Presentation.Media;
using Microsoft.SPOT.Presentation.Shapes;
using Microsoft.SPOT.Touch;
using Gadgeteer.Networking;
using GT = Gadgeteer;
using GTM = Gadgeteer.Modules;
using Gadgeteer.Modules.GHIElectronics;
using Json.NETMF;
using System.Text;
using System.IO;
using GHI.Glide;
using GHI.Glide.Display;
using GHI.Glide.UI;
using System.Net.Sockets;
using System.Collections;



namespace CashRegisterClient
{
    public partial class Program
    {
        static GHI.Glide.Display.Window mainWindow;
        GHI.Glide.UI.Button btnRetake;
        GHI.Glide.UI.Button btnOK;
        Bitmap picture;
        Socket sock;
        ArrayList products;
        int total_points;
        Customer customer;

        // This method is run when the mainboard is powered up or reset.   
        void ProgramStarted()
        {
            Debug.Print("ciao!");
            ethernetJ11D.UseThisNetworkInterface();
            ethernetJ11D.UseStaticIP("10.0.32.2", "255.255.255.252", "10.0.32.1");

            button.ButtonPressed += camera_TakePicture;
            camera.BitmapStreamed += camera_BitmapStreamed;

             start(null);
        }

        private void start(object sender)
        {
            total_points = 0;
            products = new ArrayList();
            customer = null;

            mainWindow = GlideLoader.LoadWindow(Resources.GetString(Resources.StringResources.FidelityCardUI));

            GlideTouch.Initialize();
            Glide.FitToScreen = true;

            GHI.Glide.UI.Button btnYes = (GHI.Glide.UI.Button)mainWindow.GetChildByName("Yes");
            btnYes.TapEvent += OnTapYes;

            GHI.Glide.UI.Button btnNo = (GHI.Glide.UI.Button)mainWindow.GetChildByName("No");
            //btnNo.TapEvent += OnTapProduct;
            btnNo.TapEvent += OnTapProduct;
            //Microsoft.SPOT.Presentation.Controls.Image saveImage = new Microsoft.SPOT.Presentation.Controls.Image(new Bitmap(Resources.GetBytes(Resources.BinaryResources.Sfondo), Bitmap.BitmapImageType.Jpeg));
            //mainWindow.BackImage = saveImage.Bitmap;
            mainWindow.BackImage = new Bitmap(Resources.GetBytes(Resources.BinaryResources.Sfondo), Bitmap.BitmapImageType.Jpeg);
            Glide.MainWindow = mainWindow;

        }
        

        private  void OnTapYes(object sender)
        {
            
            mainWindow = GlideLoader.LoadWindow(Resources.GetString(Resources.StringResources.ScanProduct_Card));
            btnRetake = (GHI.Glide.UI.Button)mainWindow.GetChildByName("Retake");
            btnRetake.Visible = false;
            btnOK = (GHI.Glide.UI.Button)mainWindow.GetChildByName("ok");
            btnOK.Visible = false;

            btnRetake.TapEvent += OnTapRetake;
            btnOK.TapEvent += OnTapOK;
            
            Glide.MainWindow = mainWindow;
            camera.StartStreaming();

        }

        void camera_BitmapStreamed(Camera sender, Bitmap e)
        {
            GHI.Glide.UI.Image photo = (GHI.Glide.UI.Image)mainWindow.GetChildByName("Photo");
            photo.Bitmap = e;
            picture = e;
            //photo.Bitmap = new Bitmap(Resources.GetBytes(Resources.BinaryResources.Sfondo), Bitmap.BitmapImageType.Jpeg);
            photo.Render();
            Glide.Flush(photo);

            //displayTE35.SimpleGraphics.DisplayImage(e, 5, 33);
            //sender.StopStreaming();
        }

        void camera_TakePicture(Gadgeteer.Modules.GHIElectronics.Button sender, Gadgeteer.Modules.GHIElectronics.Button.ButtonState state)
        {
            camera.StopStreaming();
            btnRetake.Visible = true;
            btnOK.Visible = true;
            Glide.MainWindow = mainWindow;
        }

        private void OnTapOK(object sender)
        {
            mainWindow = GlideLoader.LoadWindow(Resources.GetString(Resources.StringResources.Loading));
            mainWindow.BackImage = new Bitmap(Resources.GetBytes(Resources.BinaryResources.hourglassL), Bitmap.BitmapImageType.Jpeg);
            Glide.MainWindow = mainWindow;
            //Thread.Sleep(5);

            mainWindow = GlideLoader.LoadWindow(Resources.GetString(Resources.StringResources.User_details));
            //mainWindow.BackImage = new Bitmap(Resources.GetBytes(Resources.BinaryResources.hourglassL), Bitmap.BitmapImageType.Jpeg);
            TextBox details = (GHI.Glide.UI.TextBox)mainWindow.GetChildByName("details");
            GHI.Glide.UI.Button btnRetry = (GHI.Glide.UI.Button)mainWindow.GetChildByName("retry");
            GHI.Glide.UI.Button btnSkip = (GHI.Glide.UI.Button)mainWindow.GetChildByName("skip");
            btnRetry.TapEvent += OnTapYes;
            btnSkip.TapEvent += OnTapProduct;
            GHI.Glide.UI.TextBox textboxError = (GHI.Glide.UI.TextBox)mainWindow.GetChildByName("error");

            string retVal = null;
            using (var req = HttpWebRequest.Create("http://10.0.32.1:8000/getCustomerByIDWS"))
            {
                try
                {
                    req.Method = "POST";
                    req.ContentType = "application/json; charset=utf-8";
                    GetCustomerByIDWSRequest reqobj = new GetCustomerByIDWSRequest();
                    reqobj.code = 3;//random

                    string jsonText = JsonSerializer.SerializeObject(reqobj);
                    byte[] byteArray = Encoding.UTF8.GetBytes(jsonText);
                    req.ContentLength = byteArray.Length;
                    Stream dataStream = req.GetRequestStream();
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    dataStream.Close();

                    //TODO: scegliere dove aprire il socket
                    sock = Functions.connect("Bitmap Conn", "10.0.32.1", 19000);
                    if (sock == null)
                    {
                        throw new SocketException(SocketError.HostUnreachable);
                    }
                    Functions.sendBitmap(picture, sock);
                    sock.Close();

                    retVal = Functions.handleResponse(req);
                }
                catch (SocketException)
                {
                    textboxError.Text = "Host unreachable!";
                    details.Visible = false;
                    btnRetry.Visible = true;
                    mainWindow.BackImage = new Bitmap(Resources.GetBytes(Resources.BinaryResources.Sfondo), Bitmap.BitmapImageType.Jpeg);
                    Glide.MainWindow = mainWindow;
                    return;
                }
            }

            if(retVal != null)
            {
                Hashtable hasht = JsonSerializer.DeserializeString(retVal) as Hashtable;

                Customer c = new Customer(hasht["customerID"].ToString(), hasht["name"].ToString(), int.Parse(hasht["points"].ToString()), hasht["mail"].ToString());
                btnSkip.Text = "Skip"; 

                if(c.customerID.CompareTo("error") == 0)
                {
                    details.Visible = false;
                    btnRetry.Visible = true;
                    if (c.points == -1)
                    {
                        //sql exc

                        textboxError.Text = "Server error!";                     
                    }
                    else if(c.points == -3)
                    {
                        //product not found: foto errata o prodotto non in db

                        btnRetry.Visible = true;
                        textboxError.Text = "Customer not in the DB!";
                    }
                    else
                    {
                        //prodotto non riconosciuto
                        textboxError.Text = "Customer not recognized!";
                    }
                }
                else
                {
                    //user riconosciuto
                    TextBox error = (GHI.Glide.UI.TextBox)mainWindow.GetChildByName("error");
                    error.Visible = false;
                    details.Text = "ID: " + c.customerID + " Name: " + c.name + " Points: " + c.points;
                    btnRetry.Visible = false;
                    btnSkip.Text = "Continue";
                    customer = c;
                    total_points = c.points;
                }
            }
            else
            {
                //errore di rete
                details.Visible = false;
                btnSkip.Text = "Skip";
                btnRetry.Visible = true;
            }
            mainWindow.BackImage = new Bitmap(Resources.GetBytes(Resources.BinaryResources.Sfondo), Bitmap.BitmapImageType.Jpeg);
            Glide.MainWindow = mainWindow;
        }

        private void OnTapRetake(object sender)
        {
            camera.StartStreaming();
        }


        private void OnTapProduct(object sender)
        {
            
            mainWindow = GlideLoader.LoadWindow(Resources.GetString(Resources.StringResources.ScanProduct_Card));
            TextBox textbox = (GHI.Glide.UI.TextBox)mainWindow.GetChildByName("textbox");
            textbox.Text = "Focus the barcode and press the button";
            btnRetake = (GHI.Glide.UI.Button)mainWindow.GetChildByName("Retake");
            btnRetake.Visible = false;
            btnOK = (GHI.Glide.UI.Button)mainWindow.GetChildByName("ok");
            btnOK.Visible = false;

            btnRetake.TapEvent += OnTapRetake;
            btnOK.TapEvent += OnTapOkProduct;

            
            Glide.MainWindow = mainWindow;
            camera.StartStreaming();
        }

        

        private void OnTapOkProduct(object sender)
        {
            mainWindow = GlideLoader.LoadWindow(Resources.GetString(Resources.StringResources.Loading));
            mainWindow.BackImage = new Bitmap(Resources.GetBytes(Resources.BinaryResources.hourglassL), Bitmap.BitmapImageType.Jpeg);
            Glide.MainWindow = mainWindow;
            //Thread.Sleep(2000);

            mainWindow = GlideLoader.LoadWindow(Resources.GetString(Resources.StringResources.ShowProduct));
            //mainWindow.BackImage = new Bitmap(Resources.GetBytes(Resources.BinaryResources.hourglassL), Bitmap.BitmapImageType.Jpeg);
            TextBox details = (GHI.Glide.UI.TextBox)mainWindow.GetChildByName("details");
            TextBox price = (GHI.Glide.UI.TextBox)mainWindow.GetChildByName("price");
            TextBox quantity = (GHI.Glide.UI.TextBox)mainWindow.GetChildByName("quantity");
            GHI.Glide.UI.Button btnNewProduct = (GHI.Glide.UI.Button)mainWindow.GetChildByName("next");
            GHI.Glide.UI.Button btnCheckout = (GHI.Glide.UI.Button)mainWindow.GetChildByName("checkout");
            btnNewProduct.TapEvent += OnTapProduct;
            btnCheckout.TapEvent += OnTapCheckout;
            GHI.Glide.UI.TextBox textboxError = (GHI.Glide.UI.TextBox)mainWindow.GetChildByName("error");
            

            ////////////////////////////////////////
            string retVal = null;
            using (var req = HttpWebRequest.Create("http://10.0.32.1:8000/getProductbyIDWS"))
            {
                try
                {
                    req.Method = "POST";
                    req.ContentType = "application/json; charset=utf-8";
                    GetProductByIDRequest reqobj = new GetProductByIDRequest();
                    reqobj.code = 3;

                    string jsonText = JsonSerializer.SerializeObject(reqobj);
                    byte[] byteArray = Encoding.UTF8.GetBytes(jsonText);
                    req.ContentLength = byteArray.Length;
                    Stream dataStream = req.GetRequestStream();
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    dataStream.Close();

                    //TODO: scegliere dove aprire il socket
                    sock = Functions.connect("Bitmap Conn", "10.0.32.1", 19000);
                    if (sock == null)
                    {
                        throw new SocketException(SocketError.HostUnreachable);
                    }
                    Functions.sendBitmap(picture, sock);
                    sock.Close();

                    retVal = Functions.handleResponse(req);
                }
                catch (SocketException)
                {
                    textboxError.Text = "Host unreachable!";
                    details.Visible = false;
                    price.Visible = false;
                    quantity.Visible = false;
                    btnNewProduct.Text = "Retry";
                    mainWindow.BackImage = new Bitmap(Resources.GetBytes(Resources.BinaryResources.Sfondo), Bitmap.BitmapImageType.Jpeg);
                    Glide.MainWindow = mainWindow;
                    return;
                }
            }
           
            if (retVal != null)
            {
                Hashtable hasht = JsonSerializer.DeserializeString(retVal) as Hashtable;
                Product p = new Product(hasht["barcode"].ToString(), hasht["productName"].ToString(), (float)double.Parse(hasht["price"].ToString()), hasht["quantity"].ToString(), hasht["brand"].ToString(), int.Parse(hasht["extraPoints"].ToString()));
                

                if (p.barcode.CompareTo("error") == 0)
                {
                    details.Visible = false;
                    price.Visible = false;
                    quantity.Visible = false;
                    btnNewProduct.Text = "Retry";
                   
                    if (p.extraPoints == -1)
                    {
                        //sql exc             
                        textboxError.Text = "Server error!";
                    }
                    else if (p.extraPoints == -2)
                    {
                        //product not found: foto errata o prodotto non in db
                        textboxError.Text = "Product not in the DB!";
                       
                    }
                    else//ss-lenght == 0)
                    {
                        textboxError.Text = "Product not recognized!";
                    }

                }
                else
                {
                    //prodotto ricevuto correttamente
                    TextBox error = (GHI.Glide.UI.TextBox)mainWindow.GetChildByName("error");
                    error.Visible = false;


                    double a = (double)p.price;
                    
                    details.Text = "Product: " + p.productName + " " + p.brand;
                    quantity.Text = "Quantity: " + p.quantity;
                    price.Text = "Price: " + a.ToString("N");
                    total_points += p.extraPoints;
                    products.Add(p);

                }
            }
            else
            {
                //errore di rete
                btnNewProduct.Text = "Retry";
                textboxError.Text = "Network error!";
                details.Visible = false;
            }
            mainWindow.BackImage = new Bitmap(Resources.GetBytes(Resources.BinaryResources.Sfondo), Bitmap.BitmapImageType.Jpeg);
            Glide.MainWindow = mainWindow;
        }

        private void OnTapCheckout(object sender)
        {
            mainWindow = GlideLoader.LoadWindow(Resources.GetString(Resources.StringResources.Checkout));
            DataGrid grid = (GHI.Glide.UI.DataGrid)mainWindow.GetChildByName("grid");
            GHI.Glide.UI.Button btnPay = (GHI.Glide.UI.Button)mainWindow.GetChildByName("Pay");

            grid.AddColumn(new DataGridColumn("n.", 20));
            grid.AddColumn(new DataGridColumn("Products", 110));
            grid.AddColumn(new DataGridColumn("Brand", 110));
            grid.AddColumn(new DataGridColumn("Price", 50));
            int i = 1;
            float total = 0;
            foreach(Product p in products){

                double d = (double)p.price;
                DataGridItem item = new DataGridItem(new Object[4] {i, p.productName, p.brand, d.ToString("N")});
                grid.AddItem(item);
                total += p.price;
                i++;
        }
       
            total_points += (int)total;
            //azzerare tot_points e arrayList

            TextBox textTotal = (GHI.Glide.UI.TextBox)mainWindow.GetChildByName("total");
            textTotal.Text = "Total: " + total.ToString("N");

            btnPay.TapEvent += pay;
          //  mainWindow.AddChild(grid);
          //  grid.Render();
            Glide.MainWindow = mainWindow;
          //  Glide.Flush(grid);
            
        }

        private int updatePoints()
        {
            mainWindow = GlideLoader.LoadWindow(Resources.GetString(Resources.StringResources.Loading));
            mainWindow.BackImage = new Bitmap(Resources.GetBytes(Resources.BinaryResources.hourglassL), Bitmap.BitmapImageType.Jpeg);
            Glide.MainWindow = mainWindow;

            string retVal = null;
            using (var req = HttpWebRequest.Create("http://10.0.32.1:8000/updatePointsWS"))
            {
                try
                {
                    req.Method = "POST";
                    req.ContentType = "application/json; charset=utf-8";
                    UpdatePointsWSRequest reqobj = new UpdatePointsWSRequest();
                    reqobj.totalPoints = total_points;
                    reqobj.customerCode = customer.customerID;

                    string jsonText = JsonSerializer.SerializeObject(reqobj);
                    byte[] byteArray = Encoding.UTF8.GetBytes(jsonText);
                    req.ContentLength = byteArray.Length;
                    Stream dataStream = req.GetRequestStream();
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    dataStream.Close();

                    retVal = Functions.handleResponse(req);
                }
                catch (SocketException)
                {
                    return -5; //Network error
                }
            }

            if (retVal != null)
            {
                Hashtable hasht = JsonSerializer.DeserializeString(retVal) as Hashtable;

                UpdatePointsResponse upr = new UpdatePointsResponse(int.Parse(hasht["statusCode"].ToString()));

                
                if (upr.statusCode <0)
                {
                    if (upr.statusCode == -1)
                    {
                        //sql exc
                        return -1;

                    }
                    else if (upr.statusCode == -3)
                    {
                        //customer not found: foto errata o prodotto non in db

                        return -3;
                    }

                    return -2;

                }
                else
                {
                    //prodotto ricevuto correttamente
                    //start(null);
                    return 1;
                }
            }
            else
            {
                //errore di rete
                /*btnNewProduct.Text = "Retry";
                GHI.Glide.UI.TextBox textboxError = (GHI.Glide.UI.TextBox)mainWindow.GetChildByName("error");
                textboxError.Text = "Network error!";
                details.Visible = false;*/
                return -5;
            }
        }

        private void pay(object sender)
        {
            int res = 0, resM = 0;
            

            if (customer != null)
            {
                res = updatePoints();
                if (customer.mail != null && res != -5)
                {
                    resM = sendMail();
                }
            }
                
            //mainWindow.BackImage = new Bitmap(Resources.GetBytes(Resources.BinaryResources.hourglassL), Bitmap.BitmapImageType.Jpeg);
            mainWindow = GlideLoader.LoadWindow(Resources.GetString(Resources.StringResources.lastWindow));
            TextBox details = (GHI.Glide.UI.TextBox)mainWindow.GetChildByName("details");
            TextBox result = (GHI.Glide.UI.TextBox)mainWindow.GetChildByName("result");
            GHI.Glide.UI.Button btnNextCustomer = (GHI.Glide.UI.Button)mainWindow.GetChildByName("next");
            btnNextCustomer.TapEvent += start;
            details.Text = "Payment succesfull!";

            if (res <= 0 || resM <= 0 )
            {
                if(res == -5 || resM == -5)
                    result.Text = "Host unreachable";
                else
                    result.Text = "Points not updated";
            }
            else
            {
                result.Text = "Total points: "+ total_points;
            }
            //TODO: TEXT BOX PER MAIL SENT - MAIL NOT SENT
            mainWindow.BackImage = new Bitmap(Resources.GetBytes(Resources.BinaryResources.Sfondo), Bitmap.BitmapImageType.Jpeg);
            Glide.MainWindow = mainWindow;
        }

        private int sendMail()
        {
            string retVal = null;
            float total_price = 0;
            ArrayList stringlist = new ArrayList();
            using (var req = HttpWebRequest.Create("http://10.0.32.1:8000/sendMailWS"))
            {
                try
                {
                    req.Method = "POST";
                    req.ContentType = "application/json; charset=utf-8";
                    SendMailWSRequest reqobj = new SendMailWSRequest();
                    reqobj.customerCode = customer.customerID;

                    foreach (Product p in products)
                    {
                        Debug.Print(p.price.ToString("N"));
                        stringlist.Add(p.productName + " " + p.brand + "        " + p.price.ToString("N") + "€");
                        total_price += p.price;
                    }
                    reqobj.receipt = stringlist;
                    reqobj.totalPoints = total_points;
                    reqobj.totalPrice = total_price;

                    string jsonText = JsonSerializer.SerializeObject(reqobj);
                    byte[] byteArray = Encoding.UTF8.GetBytes(jsonText);
                    req.ContentLength = byteArray.Length;
                    Stream dataStream = req.GetRequestStream();
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    dataStream.Close();

                    retVal = Functions.handleResponse(req);
                }
                catch (SocketException)
                {
                    return -5; // Network error
                }
            }

            if (retVal != null)
            {
                Hashtable hasht = JsonSerializer.DeserializeString(retVal) as Hashtable;

                SendMailResponse smr = new SendMailResponse(int.Parse(hasht["statusCode"].ToString()));


                if (smr.statusCode < 0)
                {
                    if (smr.statusCode == -1)
                    {
                        //sql exc
                        return -1;

                    }
                    else if (smr.statusCode == -3)
                    {
                        //customer not found

                        return -3;
                    }

                    return -2;

                }
                else
                {
                    //prodotto ricevuto correttamente
                    return 1;
                }
            }
            else
            {
                //errore di rete
                return -5;
            }
        }
    }
}
