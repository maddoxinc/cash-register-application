<Glide Version="1.0.7">
  <Window Name="instance115" Width="320" Height="240" BackColor="FFFFFF">
    <TextBox Name="instance360" X="0" Y="0" Width="320" Height="33" Alpha="255" Text="Checkout" TextAlign="Center" Font="4" FontColor="000000"/>
    <TextBox Name="total" X="0" Y="33" Width="240" Height="32" Alpha="255" Text="" TextAlign="Left" Font="4" FontColor="000000"/>
    <DataGrid Name="grid" X="0" Y="68" Width="319" Height="170" Alpha="255" Font="4" RowCount="5" RowHeight="34" Draggable="True" TappableCells="False" SortableHeaders="False" ShowHeaders="True" ShowScrollbar="True" ScrollbarWidth="15" HeadersBackColor="707476" HeadersFontColor="FFFFFF" ItemsBackColor="FFFFFF" ItemsAltBackColor="F4F4F4" ItemsFontColor="000000" SelectedItemBackColor="FFF299" SelectedItemFontColor="000000" GridColor="BFC5C9" ScrollbarBackColor="C0C0C0" ScrollbarScrubberColor="000000"/>
    <Button Name="Pay" X="240" Y="33" Width="80" Height="32" Alpha="255" Text="Pay" Font="4" FontColor="000000" DisabledFontColor="808080" TintColor="000000" TintAmount="0"/>
  </Window>
</Glide>