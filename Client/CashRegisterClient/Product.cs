using System;
using Microsoft.SPOT;

namespace CashRegisterClient
{
    class Product
    {
        public String barcode { get; set; }
        public String brand { get; set; }
        public int extraPoints { get; set; }
        public float price { get; set; }
        public String productName { get; set; }
        public String quantity { get; set; }
        
        

        public Product() { }

        public Product(String barcode, String productName, float price, String quantity, String brand, int extraPoints)
        {
            this.barcode = barcode;
            this.productName = productName;
            this.price = price;
            this.quantity = quantity;
            this.brand = brand;
            this.extraPoints = extraPoints;
        }
    }
}
