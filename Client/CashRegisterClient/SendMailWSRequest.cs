using System;
using Microsoft.SPOT;
using System.Collections;

namespace CashRegisterClient
{
    class SendMailWSRequest
    {
        public string customerCode { get; set; }
        public ArrayList receipt { get; set; }
        public int totalPoints { get; set; }
        public float totalPrice { get; set; }
    }
}
