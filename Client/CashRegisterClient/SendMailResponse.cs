using System;
using Microsoft.SPOT;

namespace CashRegisterClient
{
    class SendMailResponse
    {
        public int statusCode { get; set; }

        public SendMailResponse(int statusCode)
        {
            this.statusCode = statusCode;
        }
    }
}
