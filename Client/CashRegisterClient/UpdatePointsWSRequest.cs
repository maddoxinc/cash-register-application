using System;
using Microsoft.SPOT;

namespace CashRegisterClient
{
    class UpdatePointsWSRequest
    {
        public string customerCode { get; set; }
        public int totalPoints { get; set; }
    }
}
