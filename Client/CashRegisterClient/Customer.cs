using System;
using Microsoft.SPOT;

namespace CashRegisterClient
{
    class Customer
    {
        public string customerID { get; set; }
        public string name { get; set; }
        public int points { get; set; }
        public string mail { get; set; }

        public Customer() { }

        public Customer(String customerID, String name, int points, String mail)
        {
            this.customerID = customerID;
            this.name = name;
            this.points = points;
            this.mail = mail;
        }

    }
}
