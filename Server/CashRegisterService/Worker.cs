﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Spire.Barcode;

namespace CashRegisterService
{
    class Worker
    {
        public Socket sock;
        public Socket sock_control;
        Stream received_bitmap_stream;

        public Worker()
        {
           
        }

        public void receiveBitmap()
        {
            while(true) {
                sock_control = accept("bitmap connection", "192.168.1.2", 19000);
                //received_bitmap_stream = receive(sock_control);
                byte[] header = new byte[54];
                FileStream fs = new FileStream("../../header.txt", FileMode.Open);
                fs.Read(header, 0, 54);
                fs.Close();
                byte[] complete_bitmap = new byte[320*240*3+54];
                Buffer.BlockCopy(header, 0, complete_bitmap, 0, header.Length);
                byte[] received_array = receive(sock_control);

                for (int i = 0, j = 54; i < 320 * 240 * 4; i += 4, j += 3)
                {
                    complete_bitmap[j + 0] = received_array[i + 2];
                    complete_bitmap[j + 1] = received_array[i + 1];
                    complete_bitmap[j + 2] = received_array[i + 0];
                }

                received_bitmap_stream = new MemoryStream(complete_bitmap);    
                
                CashRegisterServiceLib.CashRegisterOperations.passBitmap(received_bitmap_stream);
                CashRegisterServiceLib.CashRegisterOperations.setEvent();
                sock_control.Disconnect(true);
            }
        }

        public Socket accept(String conn_name, String address, int port)
        {
            SocketPermission sockperm;
            IPEndPoint iep;

            //Console.WriteLine("Initializing...\n");

            sockperm = new SocketPermission(NetworkAccess.Accept, TransportType.Tcp, address, SocketPermission.AllPorts);
            sockperm.Demand();
            //Console.WriteLine("Permission granted!\n");

           // IPHostEntry iph = Dns.GetHostEntry(address);
           // IPAddress ipa = iph.AddressList[0];
            iep = new IPEndPoint(IPAddress.Any, port);
            try
            {
                sock = new Socket(iep.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            }
            catch (SystemException ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
            //Console.WriteLine("Socket " + conn_name + " created!\n");

            try
            {
                sock.Bind(iep);
            }
            catch (System.Net.Sockets.SocketException ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
            //Console.WriteLine("Socket bound!\n");


            try
            {
                sock.Listen(1); //Quanti ne devono stare in coda??
            }
            catch (System.Net.Sockets.SocketException ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
            Console.WriteLine("Listening...\n");

            try
            {
                sock_control = sock.Accept();
                sock.Close();
                sock = null;
            }
            catch (System.Net.Sockets.SocketException ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
            Console.WriteLine("Client " + conn_name + " connected!\n");

            return sock_control;
        }

        public byte[] receive(Socket connsock)
        {
            byte[] bufferbyter;
            //Stream stream1;

            try
            {
                int bytetoread = receive_int(connsock);
                bufferbyter = new byte[bytetoread];
                receive_checked(connsock, ref bufferbyter, bytetoread);
                return bufferbyter;
              //  stream1 = new MemoryStream(bufferbyter);
               // System.IO.FileStream _fileStream = new FileStream("header.txt",FileMode.Create,FileAccess.Write);
               /* _fileStream.Write(bufferbyter, 0, 54);
                _fileStream.Close();

                System.IO.FileStream _fileStream2 = new FileStream("bitmapCompl.txt", FileMode.Create, FileAccess.Write);
                _fileStream2.Write(bufferbyter, 0, bufferbyter.Length);
                _fileStream2.Close();*/
            }
            catch (Exception)
            {
                return null;
            }

            //return stream1;
        }

        public int receive_int(Socket sock)
        {
            byte[] bufn = new byte[4];
            int res;

            try
            {
                if (!receive_checked(sock, ref bufn, 4))
                {
                    return -1;
                }
                if (BitConverter.IsLittleEndian)
                {
                    Array.Reverse(bufn);
                }

                res = BitConverter.ToInt32(bufn, 0);
            }
            catch (Exception)
            {
                return -1;
            }
            return res;
        }

        public bool receive_checked(Socket sock, ref byte[] buffer, int n)
        {
            int actual_read = 0;

            try
            {
                while (actual_read != n)
                {
                    actual_read += sock.Receive(buffer, actual_read, n - actual_read, 0);
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
    }
}
