﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.ServiceModel.Web;
using CashRegisterServiceLib;
using CashRegisterServiceDB;
using System.ServiceModel.Description;
using System.Threading;
using System.Drawing;
using System.IO;


namespace CashRegisterService
{
    class Program
    {
        public static Thread thread_bitmap;
        //public static Stream received_bitmap;
        static void Main(string[] args)
        {


            CashRegisterOperations CRServices = new CashRegisterOperations();
            //WebServiceHost _serviceHost = new WebServiceHost(CRServices, new Uri("http://localhost:8000/CRService"));
            WebServiceHost _serviceHost = new WebServiceHost(CRServices, new Uri("http://localhost:8000/"));
            ServiceEndpoint ep = _serviceHost.AddServiceEndpoint(typeof(ICashRegisterOperations), new WebHttpBinding(), "");
            ServiceDebugBehavior sdb = _serviceHost.Description.Behaviors.Find<ServiceDebugBehavior>();
            sdb.HttpHelpPageEnabled = false;
            _serviceHost.Open();

            Worker worker = new Worker();
            thread_bitmap = new Thread(worker.receiveBitmap);
            thread_bitmap.SetApartmentState(ApartmentState.STA);

            // Start the worker thread.
            thread_bitmap.Start();
            
            Console.WriteLine("Service OK!");
            Console.ReadKey();
            _serviceHost.Close();

        }


    }
}
