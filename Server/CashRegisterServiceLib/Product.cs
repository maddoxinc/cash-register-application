﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CashRegisterServiceLib
{
    public class Product
    {

        public int barcode { get; private set; }
        public String productName { get; private set; }
        public float price { get; private set; }
        public String quantity { get; private set; }
        public String brand { get; private set; }
        public int extraPoints { get; private set; }


        public Product(int barcode, String productName, float price, String quantity, String brand, int extraPoints)
        {
            this.barcode = barcode;
            this.productName = productName;
            this.price = price;
            this.quantity = quantity;
            this.brand = brand;
            this.extraPoints = extraPoints;
        }

    }
}
