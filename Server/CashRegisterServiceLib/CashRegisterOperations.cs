﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.ServiceModel.Activation;

using System.IO;
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;
using CashRegisterServiceDB;
using System.Drawing;
using System.Data.SqlClient;
using Spire.Barcode;


namespace CashRegisterServiceLib
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Single, IncludeExceptionDetailInFaults = true)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class CashRegisterOperations : ICashRegisterOperations
    {
        static public AutoResetEvent ev = new AutoResetEvent(false);
        static public Bitmap received_bitmap;

        static public void setEvent() {
            ev.Set();
        }

        static public void passBitmap(Stream received_bitmap_stream)
        {
            received_bitmap = new Bitmap(received_bitmap_stream);
        }

        public Product getProductbyIDWS(Stream request)
        {
            string id = null;
            Product p = null;
            //string s = read.ReadToEnd();

            DataContractJsonSerializer serr = new DataContractJsonSerializer(typeof(GetProductByIDWSRequest));
            GetProductByIDWSRequest req = (GetProductByIDWSRequest)serr.ReadObject(request);
            Console.WriteLine(req.code);

            ev.WaitOne();

            string[] ss =  BarcodeScanner.Scan(received_bitmap, BarCodeType.EAN13);
            if (ss.Length > 0)
            {
                Console.WriteLine(ss[0]);
                id = ss[0];
                try
                {
                    DatabaseOperations dbo = new DatabaseOperations();
                    p = dbo.getProductsByID(id);
                }
                catch (SqlException ex)
                {
                    Console.WriteLine(ex.Message);
                    p = new Product("error", "null", 0, "null", "null", -1);
                }
                catch (ProductNotFoundException ex)
                {
                    Console.WriteLine(ex.Message);
                    p = new Product("error", "null", 0, "null", "null", -2);
                }
            }
            else
            {
                p = new Product("error","null",0,"null","null",-4);
            }



            return p;

        }

        public Customer getCustomerByIDWS(Stream request)
        {
            string id = null;
            Customer c = null;

            DataContractJsonSerializer serr = new DataContractJsonSerializer(typeof(GetProductByIDWSRequest));
            GetProductByIDWSRequest req = (GetProductByIDWSRequest)serr.ReadObject(request);
            //Console.WriteLine(req.code);

            ev.WaitOne();

            string[] ss = BarcodeScanner.Scan(received_bitmap, BarCodeType.QRCode);
            if (ss.Length > 0)
            {
                Console.WriteLine(ss[0]);
                id = ss[0];
                try
                {
                    DatabaseOperations dbo = new DatabaseOperations();
                    c = dbo.getCustomerByID(id);
                }
                catch (SqlException ex)
                {
                    Console.WriteLine(ex.Message);
                    c = new Customer("error","null",-1, "null");
                }
                catch (CustomerNotFoundException ex)
                {
                    Console.WriteLine(ex.Message);
                    c = new Customer("error","null",-3, "null");
                }
            }
            else
            {
                c = new Customer("error", "null", -5, "null");
            }


            return c;
        }

        public UpdatePointsResponse updatePointsWS(Stream request)
        {
            UpdatePointsResponse resp = null;

            DataContractJsonSerializer serr = new DataContractJsonSerializer(typeof(UpdatePointsWSRequest));
            UpdatePointsWSRequest req = (UpdatePointsWSRequest)serr.ReadObject(request);
            Console.WriteLine("Customer code: " + req.customerCode);

            try
            {
                DatabaseOperations dbo = new DatabaseOperations();
                dbo.updatePoints(req.customerCode, req.totalPoints);
                resp = new UpdatePointsResponse();
                resp.statusCode = 1;
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                resp = new UpdatePointsResponse();
                resp.statusCode = -1;
            }
            catch (CustomerNotFoundException ex)
            {
                Console.WriteLine(ex.Message);
                resp = new UpdatePointsResponse();
                resp.statusCode = -3;
            }

            return resp;
        }

        public SendMailResponse sendMailWS(Stream request)
        {
            SendMailResponse resp = null;
            Customer c = null;

            DataContractJsonSerializer serr = new DataContractJsonSerializer(typeof(SendMailWSRequest));
            SendMailWSRequest req = (SendMailWSRequest)serr.ReadObject(request);
            Console.WriteLine("Customer code: " + req.customerCode);

            try
            {
                DatabaseOperations dbo = new DatabaseOperations();
                c = dbo.getCustomerByID(req.customerCode);
                //Chiamare funzione creazione PDF e mail e catchare eventuali errori
                Functions.createPDFReceipt(req.receipt, req.totalPoints, req.totalPrice);
                Functions.sendMail(c);
                resp = new SendMailResponse();
                resp.statusCode = 1;
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                resp = new SendMailResponse();
                resp.statusCode = -1;
            }
            catch (CustomerNotFoundException ex)
            {
                Console.WriteLine(ex.Message);
                resp = new SendMailResponse();
                resp.statusCode = -3;
            }

            return resp;
        }
    }
}