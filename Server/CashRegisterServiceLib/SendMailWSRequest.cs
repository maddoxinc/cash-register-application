﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Collections;

namespace CashRegisterServiceLib
{
    [DataContract]
    public class SendMailWSRequest
    {
        [DataMember]
        public string customerCode { get; set; }
        [DataMember]
        public ArrayList receipt { get; set; }
        [DataMember]
        public int totalPoints { get; set; }
        [DataMember]
        public float totalPrice { get; set; }
    }
}
