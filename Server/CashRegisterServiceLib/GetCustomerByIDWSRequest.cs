﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace CashRegisterServiceLib
{
    [DataContract]
    class GetCustomerByIDWSRequest
    {
      [DataMember]
      public int code { get; set; }
    }
}
