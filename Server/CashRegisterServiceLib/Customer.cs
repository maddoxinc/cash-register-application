﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CashRegisterServiceLib
{
    public class Customer
    {
        public int customerID { get; private set; }
        public String name { get; private set; }
        public int points { get; private set; }


        public Customer(int customerID, String name, int points){
            this.customerID = customerID;
            this.name = name;
            this.points = points;
        }
    }
}
