﻿using CashRegisterServiceDB;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;

namespace CashRegisterServiceLib
{
    public static class Functions
    {
        public static bool createPDFReceipt(ArrayList products, int total_points, float total_price)
        {
            // step 1: creation of a document-object
            Document document = new Document();

            try
            {
                PdfWriter.GetInstance(document, new FileStream("receiptTmp.pdf", FileMode.OpenOrCreate));

                document.Open();

                Paragraph p = new Paragraph("GHI SuperMarket");
                p.Alignment = 1;
                document.Add(p);
                p = new Paragraph("Corso Duca degli Abruzzi, 27");
                p.Alignment = 1;
                document.Add(p);
                List l = new List(true);
                for (int i = 0; i < products.Count; i++)
                {
                    l.Add(new ListItem((string)products[i]));
                }

                document.Add(l);
                document.Add(new Paragraph("---------------"));
                document.Add(new Paragraph("Total: " + total_price.ToString("C", CultureInfo.CurrentCulture)));
                document.Add(new Paragraph("Total Points: " + total_points));

            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
            finally
            {
                document.Close();
            }
            return true;
        }

        public static bool sendMail(Customer c)
        {
            MailMessage myMailMessage = new MailMessage();
            SmtpClient myMailClient = null;
            myMailMessage.To.Add(new MailAddress(c.mail)); //destinatario
            myMailMessage.From = new MailAddress("ghisupermarket@gmail.com", "GHI SuperMarket"); //mittente
            myMailMessage.IsBodyHtml = true;


            string pdf = "receiptTmp.pdf";
            //string path_preventivo = Path.Combine(TargetPath, user, param[1].ToString(), pdf);
            Attachment data = new Attachment(pdf, MediaTypeNames.Application.Pdf);
            ContentDisposition disposition = data.ContentDisposition;
           /* disposition.CreationDate = System.IO.File.GetCreationTime(path_preventivo);
            disposition.ModificationDate = System.IO.File.GetLastWriteTime(path_preventivo);
            disposition.ReadDate = System.IO.File.GetLastAccessTime(path_preventivo);*/
            myMailMessage.Attachments.Add(data);


            // completo il messaggio
            myMailMessage.Subject = "Receipt for " + c.name;
            myMailMessage.Body = "In attachment you can find your receipt.<br/><br/>";

            myMailClient = new SmtpClient(); //nuova istanza della classe system.net.mail.smtpclient
            myMailClient.Host = "smtp.gmail.com";
            myMailClient.Port =  587;
            myMailClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            myMailClient.EnableSsl = true;

            myMailClient.Credentials = new System.Net.NetworkCredential("ghisupermarket@gmail.com","fezspider");
            
            myMailClient.Timeout = 30000;
            myMailClient.Send(myMailMessage);
            return true;
        }
    }
}
