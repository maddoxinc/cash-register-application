﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

namespace CashRegisterServiceLib
{
    [DataContract]
    public class UpdatePointsResponse
    {
        [DataMember]
        public int statusCode { get; set; }
    }
}
