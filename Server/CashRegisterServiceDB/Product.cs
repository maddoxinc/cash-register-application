﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

namespace CashRegisterServiceDB
{
     [DataContract]
    public class Product
    {
        [DataMember]
        public String barcode { get; set; }
        [DataMember]
        public String brand { get; set; }
        [DataMember]
        public int extraPoints { get; set; }
        [DataMember]
        public float price { get; set; }
        [DataMember]
        public String productName { get;  set; }
        [DataMember]
        public String quantity { get;  set; }



        public Product() { }

        public Product(String barcode, String productName, float price, String quantity, String brand, int extraPoints)
        {
            this.barcode = barcode;
            this.productName = productName;
            this.price = price;
            this.quantity = quantity;
            this.brand = brand;
            this.extraPoints = extraPoints;
        }

    }
}
