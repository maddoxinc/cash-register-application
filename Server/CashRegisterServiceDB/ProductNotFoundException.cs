﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CashRegisterServiceDB
{
    public class ProductNotFoundException : System.ApplicationException
    {
        public ProductNotFoundException() { }
        public ProductNotFoundException(string message) { }
        public ProductNotFoundException(string message, System.Exception innner) { }
        
    }
}
