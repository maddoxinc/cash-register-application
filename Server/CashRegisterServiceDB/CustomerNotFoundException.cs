﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CashRegisterServiceDB
{
    public class CustomerNotFoundException : System.ApplicationException
    {
        public CustomerNotFoundException() { }
        public CustomerNotFoundException(string message) { }
        public CustomerNotFoundException(string message, System.Exception innner) { }
    }
}
