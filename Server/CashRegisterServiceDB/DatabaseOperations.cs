﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data;
using System.Data.Odbc;



namespace CashRegisterServiceDB
{
    public class DatabaseOperations
    {

        public Product getProductsByID(String ID)
        {
            Product selected = null;
            String productName, quantity, brand;
            float price;
            int extraPoints=0, percentage;
            
            try
            {
                DataSetShopTableAdapters.ProductsTableAdapter ProductsTA = new DataSetShopTableAdapters.ProductsTableAdapter();
                DataSetShop.ProductsDataTable productsDt = ProductsTA.GetDataBy(ID);

                if (productsDt.Count == 0)
                {
                    Console.WriteLine("product doesn't exist");
                    throw new ProductNotFoundException();
                }
                else
                {
                    Console.WriteLine("product returned!");

                    DataRow row = productsDt.Rows[0];

                    //       Console.WriteLine("name: " + row[1] + " price: " + row[2] + " quantity: " + row[3] + " brand: " + row[4]);

                    productName = row[1].ToString();
                    price = float.Parse(row[2].ToString());
                    quantity = row[3].ToString();
                    brand = row[4].ToString();

                    //     Console.WriteLine("name: " + productName + " price: " + price.ToString() + " quantity: " + quantity + " brand: " + brand);

                    //check possible promotions
                    DataSetShopTableAdapters.PromotionsTableAdapter PromotionsTA = new DataSetShopTableAdapters.PromotionsTableAdapter();
                    DataSetShop.PromotionsDataTable promotionsDt = PromotionsTA.GetDataByID(ID);

                    if (promotionsDt.Count == 0)
                    {
                        Console.WriteLine("nessun sconto");
                    }
                    else
                    {
                        Console.WriteLine("sconto trovato");


                        row = promotionsDt.Rows[0];

                        //       Console.WriteLine(" percentage: " + row[1] + " punti bonus:" + row[2]);

                        if (row[1] != null)
                        {
                            percentage = int.Parse(row[1].ToString());
                            price -= (price / 100 * percentage);
                            Console.WriteLine("discounted price: " + price);
                        }

                        if (row[2] != null)
                        {

                            extraPoints = int.Parse(row[2].ToString());

                        }

                    }

                    selected = new Product(ID, productName, price, quantity, brand, extraPoints);

                }
            }
            catch(SqlException sqlE){
                throw sqlE;
            }


            return selected;
        }

        public Customer getCustomerByID(String ID)
        {
            Customer selected = null;
            String name;
            String mail;
            int points;

            try
            {
                DataSetShopTableAdapters.CustomersTableAdapter CustomersTA = new DataSetShopTableAdapters.CustomersTableAdapter();
                DataSetShop.CustomersDataTable customersDt = CustomersTA.GetDataByID(ID);

                if (customersDt.Count == 0)
                {
                    Console.WriteLine("customer doesn't exist");
                    throw new CustomerNotFoundException();
                }
                else
                {
                    Console.WriteLine("customer found");

                    DataRow row = customersDt.Rows[0];

                    // Console.WriteLine("name: " + row[1] + " points: " + row[2]);

                    name = row[1].ToString();
                    points = int.Parse(row[2].ToString());
                    mail = row[3].ToString();
                    // Console.WriteLine("name: " + name + " points: " + points);

                    selected = new Customer(ID, name, points, mail);

                }
            }
            catch (SqlException sqlE)
            {
                throw sqlE;
            }

            return selected;
        }

        public void updatePoints(String ID, int points)
        {
            try
            {
                DataSetShopTableAdapters.CustomersTableAdapter CustomersTA = new DataSetShopTableAdapters.CustomersTableAdapter();
                int result = CustomersTA.UpdatePointsQuery(points, ID);
                if (result == 1)
                {
                    Console.WriteLine("update eseguito!");
                }
                else
                {
                    Console.WriteLine("errore update");
                    throw new CustomerNotFoundException();
                }
            }
            catch (SqlException sqlE)
            {
                throw sqlE;
            }
        }

    }
}
