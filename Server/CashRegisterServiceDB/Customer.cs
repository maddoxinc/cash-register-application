﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

namespace CashRegisterServiceDB
{
    [DataContract]
    public class Customer
    {
        [DataMember]
        public String customerID { get; set; }
        [DataMember]
        public String name { get; set; }
        [DataMember]
        public int points { get; set; }
        [DataMember]
        public String mail { get; set; }

        public Customer() { }

        public Customer(String customerID, String name, int points, String mail){
            this.customerID = customerID;
            this.name = name;
            this.points = points;
            this.mail = mail;
        }
    }
}
